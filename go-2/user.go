package main

import (
	"fmt"
	"net/http"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var db *gorm.DB
var err error

type User struct {
	gorm.Model
	Name string
	Email string
}

func InitialMigration() {
	db, err = gorm.Open("sqlite3", "test.db")
	
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to database")
	}
	
	defer db.Close()
	db.AutoMigrate(&User{})
}

func AllUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "All users")

	db, err = gorm.Open("sqlite3", "test.db")	
	if(err != nil){
		panic("Could not connect to the database")
	}
	
	defer db.Close();

	var users []User
    db.Find(&users)
    json.NewEncoder(w).Encode(users);

}

func NewUser(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "New User")

	db, err = gorm.Open("sqlite3", "test.db")
	if(err != nil){
		panic("Could not connect to the database")
	}
	
	defer db.Close();

	vars := mux.Vars(r)
	name := vars["name"]
	email := vars["email"]

    db.Create(&User {Name:name, Email: email})
   
	fmt.Fprintf(w, "New User..... Sucess")

}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Delete User")
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Update User")
}