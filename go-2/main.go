package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Home page Endpoint")
}

func handleRequests() {
	fmt.Printf("handle requests")
	
	myRouter := mux.NewRouter().StrictSlash(true)
	
	myRouter.HandleFunc("/", homePage).Methods("GET");
	myRouter.HandleFunc("/users", AllUsers).Methods("GET");
	myRouter.HandleFunc("/user/{name}/{email}", NewUser).Methods("POST");
	myRouter.HandleFunc("/user{name}", DeleteUser).Methods("DELETE")
	myRouter.HandleFunc("/user{name}/{email}", UpdateUser).Methods("PUT")
	
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

func main() {
	fmt.Printf("Main ...")
	InitialMigration();
	handleRequests()
}